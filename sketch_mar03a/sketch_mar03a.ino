
#include <EEPROM.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

//#include <eeprom_home>


#define SSID_ADDR 10
#define PSWD_ADDR 0

#define EEPROM_SIZE 512

#ifndef DEFAULT_SSID
#define DEFAULT_SSID "КотьяКормушка"
#define DEFAULT_PSK  ""
#endif

struct WifiConfig {
  String SSID;
  String PASWD;
};

WifiConfig wifiConfig;

// on start ttl frq 74880
// before ini ttl frq 115200


const char *def_ssid = DEFAULT_SSID;
const char *def_pswd = DEFAULT_PSK;

const int ssid_addr = SSID_ADDR;
int password_addr = PSWD_ADDR;

const uint8_t DELAY_FOR_CHACK_SERVER = 50000;

const char* host = "esp.k7vta3.top";
const uint16_t port = 80;

const String homePage = "<!DOCTYPE HTML>\
<html>\
<head>\
  <title>Configure your КотьяКормушка's WiFi</title>\
</head>\
<body>\
  <h1>You are connected</h1><br />\
  <form action='/' method='POST'>\
    <label for='ssid'>SSID</label><input type='text' name='ssid' id='ssid' /><br />\
    <label for='pswd'>Password</label><input type='password' name='pswd' id='pswd' /><br />\
    <input type='submit' />\
  </form>\
</body>\
</html>"; 


ESP8266WebServer server(80);
bool isLinked = false;


void setup() {
  Serial.begin(115200);
  EEPROM.begin(EEPROM_SIZE);
  delay(1000);
  Serial.println("");
  if(hasWiFiConfig()) {
    String ssid = getSSID();
    String pswd = getPSWD();
    printf("WiFi credentials: %s / %s \n", ssid.c_str(), pswd.c_str());
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, pswd);
      // Wait for connection
    byte retry = 0;
    while (WiFi.status() != WL_CONNECTED) {
      retry++;
      delay(500);
      Serial.print(".");
      if (retry > 20) {
        clearEEPROM();
        ESP.restart();
        Serial.println("Restarting...(retry is done)");
      } 
    }
    Serial.println("");
    Serial.print("Connected to ");
    Serial.println(ssid);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
    isLinked = true;
  } else {
    WiFi.softAP(def_ssid, def_pswd);
    
    IPAddress myIP = WiFi.softAPIP();
    Serial.print("AP IP address: ");
    Serial.println(myIP);
    server.on("/", handleRoot);
    server.begin();
    Serial.println("HTTP server started");
  }
  
}
byte i = 0;
void loop() {

  
  
  if(isLinked) {
    i++;
    Serial.println("Check server");
    printf("Check server: %i \n", i);
    delay(5000);
    // every 1 min clear EEPROM and restart 
    if (i > 12 * 1) {
      clearEEPROM();
      ESP.restart();
      Serial.println("Restarting...(debug)");
    }
  } else {
    server.handleClient();
  }
}

void handleRoot() {
  if (server.method() == HTTP_POST) {
    String ssid = server.arg("ssid");
    wifiConfig.SSID = ssid;
    
    char ssid_c[ssid.length()+1];
    ssid.toCharArray(ssid_c, ssid.length()+1);
    
    String pswd = server.arg("pswd");
    wifiConfig.PASWD = pswd;
    
    char pswd_c[pswd.length()+1];
    pswd.toCharArray(pswd_c, pswd.length()+1);
    
    if (ssid.length() > 0) {
      setWiFiConfig(ssid_c, pswd_c);
      ESP.restart();
      Serial.println("Restarting..");
    }
    server.send(200, "text/html", "");
  } else {
    server.send(200, "text/html", homePage);
  }
  
}

bool hasWiFiConfig() {
  int size = EEPROM.read(ssid_addr);
  return (size > 0);
}

String getSSID() {
  return getEEPROMString(ssid_addr);
}

int getSSIDLength(int ssid_addr) {
  int length = EEPROM.read(ssid_addr);
  return length;
}

String getPSWD() {
  if (password_addr == 0) {
    password_addr = ssid_addr + 3 + getSSIDLength(ssid_addr);
  }
  return getEEPROMString(password_addr);
}

void setWiFiConfig(char* ssid_in, char* pswd_in) {
  int len = setEEPROMString(ssid_addr, ssid_in);
  setEEPROMString(len + 2, pswd_in);
}
