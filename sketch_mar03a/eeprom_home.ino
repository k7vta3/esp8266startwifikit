
int setEEPROMString(int addr, char* str) {
  int len = strlen(str);
  EEPROM.write(addr, len);
  //printf("Size of SSID: %d(%d) \n", len, addr);
  addr = addr + 1;
  for (int i =  0; i < len; i++) {
    EEPROM.write(addr + i, str[i]);
    //printf("\tSet to addr: %i symbol: %c \n", addr + i, str[i]);
  }
  EEPROM.write(addr + len, 0);
  if (EEPROM.commit()) {
    Serial.println("EEPROM successfully committed");
  } else {
    Serial.println("ERROR! EEPROM commit failed");
  } 
  return  addr + len;
}

String getEEPROMString(int addrOffset) {
  int newStrLen = EEPROM.read(addrOffset);
  //printf("Size: %i arrd: %i \n", newStrLen, addrOffset);
  char str[newStrLen];
  for (int i = 0; i < newStrLen; i++)
  {
    str[i] = (char) EEPROM.read(addrOffset + 1 + i);
    //printf("\tGet from addr: %i (%i) symbol: %c \n", (addrOffset + 1 + i), i, str[i]);
  }
  str[newStrLen] = '\0';
  //printf("Get %s len: %d / %d \n", str, strlen(str), newStrLen);
  return String(str);
}

void clearEEPROM() {
  for (int i = 0; i < EEPROM_SIZE; i++) {
    EEPROM.write(i, 0);
  }  
  EEPROM.commit();
}
